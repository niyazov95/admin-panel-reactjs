import {IMenuItem} from '../model/menuItem.interface';

export const MenuItemsConst: IMenuItem[] = [
  {
    label: 'Dashboard',
    link: 'dashboard',
    icon: 'dashboard',
  },
  {
    label: 'Languages',
    link: 'languages',
    icon: 'g_translate',
  },
  {
    label: 'Words',
    link: 'words',
    icon: 'spellcheck',
  },
  {
    label: 'Statuses',
    link: 'status',
    icon: 'announcement',
  },
  {
    label: 'Links',
    link: 'link',
    icon: 'link',
  },
  {
    label: 'FAQ',
    link: 'faq',
    icon: 'live_help',
  },
  {
    label: 'Tutorials',
    link: 'tutorial',
    icon: 'ondemand_video',
  },
  {
    label: 'News',
    link: 'news',
    icon: 'receipt',
  },
  {
    label: 'Vacancies',
    link: 'vacancy',
    icon: 'work_outline',
  },
];
