import {IMenuItem} from '../model/menuItem.interface';

export const ModuleMenuItemsConst: IMenuItem[] = [
  {
    label: 'Organizations',
    link: 'organizations',
    icon: 'account_balance',
  },
  {
    label: 'Applications',
    link: 'applications',
    icon: 'apps',
  },
  {
    label: 'Elements',
    link: 'elements',
    icon: 'crop_3_2',
  },
];
