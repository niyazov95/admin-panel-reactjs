import React, {useContext} from 'react';
import clsx from 'clsx';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import {MenuItemsConst} from './const/menuItems.const';
import {IMenuItem} from './model/menuItem.interface';
import {Sidebar} from './components/sidebar';
import {Header} from './components/header';
import {WrapperContext} from '../index';
import {IWrapperContext} from './model/wrapperContext.interface';
import {NavigationWrapper} from './components/navigationWrapper';
import {Typography} from '@material-ui/core';
import {ModuleMenuItemsConst} from "./const/moduleMenuItems.const";

export const drawerWidthConst = 240;

export const useStylesConst = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidthConst,
    width: `calc(100% - ${drawerWidthConst}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidthConst,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidthConst,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  logo: {
    width: '100%',
    height: '70px',
    objectFit: 'contain',
    padding: '5px 0 5px 60px',
  },
}));

export default function LayoutWrapper() {
  let ctx = useContext<IWrapperContext>(WrapperContext);
  const menuItems: IMenuItem[] = MenuItemsConst;
  const moduleMenuItemsConst: IMenuItem[] = ModuleMenuItemsConst;
  const classes = useStylesConst();
  const theme = useTheme();
  const handleDrawerClose = () => {
    ctx.toggleDrawer?.(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header />

      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: ctx.isOpen,
          [classes.drawerClose]: !ctx.isOpen,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: ctx.isOpen,
            [classes.drawerClose]: !ctx.isOpen,
          }),
        }}>
        <div className={classes.toolbar}>
          <img
            className={classes.logo}
            src="https://apiprivate.procure.az/api/profile/GetProfilePhoto/53fb13c9-b30d-417f-91ad-aa74bf6ead0e?date=1583155922621"
            alt=""
          />
          <IconButton onClick={handleDrawerClose}>{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}</IconButton>
        </div>
        <Divider />
        <Sidebar menuItems={menuItems} />
        <Divider />
        <Sidebar menuItems={moduleMenuItemsConst} />
        <Divider />
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <NavigationWrapper />
        <br />
        <br />
        <footer>
          <Typography align={'center'}>&copy; 2020 Smart Solutions Group</Typography>
        </footer>
      </main>
    </div>
  );
}
