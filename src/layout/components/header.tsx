import React, {useContext, useEffect, useState} from 'react';
import clsx from 'clsx';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {Grid} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import {WrapperContext} from '../../index';
import {IWrapperContext} from '../model/wrapperContext.interface';
import {useStylesConst} from '../layoutWrapper';
import {getCurrentRoute, listen} from '../../router/router';
import Box from '@material-ui/core/Box';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Fade from '@material-ui/core/Fade';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';

export const Header: React.FC = () => {
  const [route, setRoute] = useState(getCurrentRoute());
  useEffect(() => listen(setRoute), []);
  const classes = useStylesConst();
  let ctx = useContext<IWrapperContext>(WrapperContext);
  const handleDrawerOpen = () => {
    ctx.toggleDrawer(true);
  };
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: ctx.isOpen,
        })}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: ctx.isOpen,
            })}>
            <MenuIcon />
          </IconButton>
          <Grid item xs={6}>
            <Typography variant="h6" noWrap>
              {(route.name as string).toUpperCase()}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Box width="100%" display="flex" justifyContent="flex-end" alignItems="center">
              <div>
                <Button endIcon={<Icon>keyboard_arrow_down</Icon>} color="inherit" onClick={handleClick}>
                  Select project
                </Button>
                <Menu id="fade-menu" anchorEl={anchorEl} keepMounted open={open} onClose={handleClose} TransitionComponent={Fade}>
                  <MenuItem onClick={handleClose}>Project 1</MenuItem>
                  <MenuItem onClick={handleClose}>Project 2</MenuItem>
                  <MenuItem onClick={handleClose}>Project 3</MenuItem>
                </Menu>
              </div>
              <div>
                <Button color="inherit" onClick={handleClick}>
                  Javid Nazarov
                  <Box ml={1}>
                    <Avatar>JN</Avatar>
                  </Box>
                </Button>
                <Menu id="fade-menu" anchorEl={anchorEl} keepMounted open={open} onClose={handleClose} TransitionComponent={Fade}>
                  <MenuItem onClick={handleClose}>Profile</MenuItem>
                  <MenuItem onClick={handleClose}>My account</MenuItem>
                  <MenuItem onClick={handleClose}>Logout</MenuItem>
                </Menu>
              </div>
            </Box>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
};
