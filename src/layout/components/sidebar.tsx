import React from 'react';
import {IMenuItem} from '../model/menuItem.interface';
import ListItem from '@material-ui/core/ListItem';
import {ListItemIcon} from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import {routes} from '../../router/router';
import Tooltip from '@material-ui/core/Tooltip';

interface ISidebar {
  menuItems: IMenuItem[];
}

export const Sidebar: React.FC<ISidebar> = (props: ISidebar) => {
  return (
    <List>
      {props.menuItems.map(menuItem => (
        <ListItem button key={menuItem.link} {...routes[menuItem.link].link()}>
          <Tooltip title={menuItem.label}>
            <ListItemIcon>
              <Icon>{menuItem.icon}</Icon>
            </ListItemIcon>
          </Tooltip>

          <ListItemText primary={menuItem.label} />
        </ListItem>
      ))}
    </List>
  );
};
