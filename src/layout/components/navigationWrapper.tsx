import React, {useEffect, useState} from 'react';
import {getCurrentRoute, listen, routes} from '../../router/router';
import {Dashboard} from '../../pages/Dashboard/dashboard';
import {WordPage} from '../../pages/Words/wordPage';
import {StatusPage} from '../../pages/Status/statusPage';
import {LinkPage} from '../../pages/Link/linkPage';
import {FaqPage} from '../../pages/FAQ/faqPage';
import {LanguagePage} from '../../pages/Language/languagePage';
import {TutorialPage} from '../../pages/Tutorial/tutorialPage';
import {NewsPage} from '../../pages/News/newsPage';
import {VacancyPage} from '../../pages/Vacancy/vacancyPage';
import {OrganizationPage} from '../../pages/organization/organization';

export const NavigationWrapper: React.FC = () => {
  const [route, setRoute] = useState(getCurrentRoute());

  useEffect(() => listen(setRoute), []);

  let page;

  if (route.name === routes.home.name) {
    page = <Dashboard />;
  } else if (route.name === routes.dashboard.name) {
    page = <Dashboard />;
  } else if (route.name === routes.words.name) {
    page = <WordPage />;
  } else if (route.name === routes.status.name) {
    page = <StatusPage />;
  } else if (route.name === routes.link.name) {
    page = <LinkPage />;
  } else if (route.name === routes.faq.name) {
    page = <FaqPage />;
  } else if (route.name === routes.languages.name) {
    page = <LanguagePage />;
  } else if (route.name === routes.tutorial.name) {
    page = <TutorialPage />;
  } else if (route.name === routes.news.name) {
    page = <NewsPage />;
  } else if (route.name === routes.vacancy.name) {
    page = <VacancyPage />;
  } else if (route.name === routes.organizations.name) {
    page = <OrganizationPage />;
  } else {
    page = <p>Not Found</p>;
  }
  return page;
};
