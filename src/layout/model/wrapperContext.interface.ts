export interface IWrapperContext {
  toggleDrawer?: (boolean) => void;
  isOpen?: boolean;
}
