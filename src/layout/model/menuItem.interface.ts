export interface IMenuItem {
  label: string;
  link: string;
  icon: string;
}
