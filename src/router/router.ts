import {createRouter, defineRoute} from 'type-route';

export const {routes, listen, getCurrentRoute} = createRouter({
  home: defineRoute('/'),
  dashboard: defineRoute('/dashboard'),
  words: defineRoute('/words'),
  languages: defineRoute('/languages'),
  status: defineRoute('/status'),
  link: defineRoute('/link'),
  faq: defineRoute('/faq'),
  tutorial: defineRoute('/tutorial'),
  news: defineRoute('/news'),
  vacancy: defineRoute('/vacancy'),
  organizations: defineRoute('/organizations'),
  applications: defineRoute('/applications'),
  elements: defineRoute('/elements'),
});
