import * as React from 'react';
import * as ReactDOM from 'react-dom';
import LayoutWrapper from './layout/layoutWrapper';
import {IWrapperContext} from './layout/model/wrapperContext.interface';

export const WrapperContext = React.createContext<IWrapperContext>(
  new (class implements IWrapperContext {
    isOpen: boolean;
    toggleDrawer: (boolean) => void;
  })()
);

class App extends React.PureComponent<{}> {
  state: IWrapperContext;
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }
  toggleDrawer = (isOpen: boolean) => {
    this.setState({
      isOpen: isOpen,
    });
  };
  render() {
    return (
      <WrapperContext.Provider value={{toggleDrawer: this.toggleDrawer, isOpen: this.state.isOpen}}>
        <LayoutWrapper />
      </WrapperContext.Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
