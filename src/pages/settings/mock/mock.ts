export const MOCK_LANG = [
  {
    languageName: 'English',
    active: true,
    default: true,
  },
  {
    languageName: 'Azerbaijan',
    active: true,
    default: false,
  },
  {
    languageName: 'Russian',
    active: false,
    default: false,
  },
];

export const MOCK_PROJECTS = [
  {
    projectName: 'E-gov (Public)',
  },
  {
    projectName: 'E-gov (Private)',
  },
  {
    projectName: 'Procure (Public)',
  },
  {
    projectName: 'Procure (Private)',
  },
  {
    projectName: 'Smart Solutions Group',
  },
];
