import React from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Switch from '@material-ui/core/Switch';
import {Grid} from '@material-ui/core';
import GTranslateIcon from '@material-ui/icons/GTranslate';
import BusinessIcon from '@material-ui/icons/Business';
import {MOCK_LANG, MOCK_PROJECTS} from './mock/mock';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    selected: {
      color: theme.palette.error.light,
    },
  })
);

export const SettingsCard: React.FC<any> = (props: any) => {
  const languages = MOCK_LANG;
  const projects = MOCK_PROJECTS;
  const classes = useStyles();
  const [checked, setChecked] = React.useState([]);
  const [selectedIndex, setSelectedIndex] = React.useState(1);

  const handleToggle = (language: any) => () => {
    language.active = !language.active;
    const newChecked = [...checked];
    setChecked(newChecked);
  };

  const handleListItemClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, index: number) => {
    setSelectedIndex(index);
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={6}>
        <List subheader={<ListSubheader>Languages</ListSubheader>} className={classes.root}>
          {languages.map(lang => (
            <ListItem divider key={lang.languageName}>
              <ListItemIcon>
                <GTranslateIcon fontSize={'small'} color={'disabled'} />
              </ListItemIcon>
              <ListItemText primary={lang.languageName} />
              <ListItemSecondaryAction>
                <Switch color={'primary'} edge="end" onChange={handleToggle(lang)} checked={lang.active} />
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </Grid>
      <Grid item xs={6}>
        <List subheader={<ListSubheader>Projects</ListSubheader>} className={classes.root}>
          {projects.map((project, index) => (
            <ListItem
              divider
              className={selectedIndex === index ? classes.selected : ''}
              dense
              key={project.projectName}
              button
              selected={selectedIndex === index}
              onClick={event => handleListItemClick(event, index)}>
              <ListItemIcon>
                <BusinessIcon fontSize={'small'} color={'disabled'} />
              </ListItemIcon>
              <ListItemText primary={project.projectName} />
            </ListItem>
          ))}
        </List>
      </Grid>
    </Grid>
  );
};
