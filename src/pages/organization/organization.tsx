import React, {useEffect} from 'react';
import axios from 'axios';

export const OrganizationPage: React.FC<any> = (props: any) => {
  const [organization, setOrganization] = React.useState([]);

  useEffect(() => {
    axios.get('http://91.250.113.169:5000/api/organization').then(r => {
      setOrganization(r.data);
    });
  });

  return (
    <div>
      <ul>
        {organization.map(o => (
          <li key={o._id}>{o.name}</li>
        ))}
      </ul>
    </div>
  );
};
