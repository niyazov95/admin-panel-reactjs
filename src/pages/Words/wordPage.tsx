import React from 'react';
import {Grid} from '@material-ui/core';
import DataTable from '../../shared/dataTable';

export const WordPage: React.FC = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <DataTable dataTableTitle={'Words'} />
        </Grid>
      </Grid>
    </>
  );
};
