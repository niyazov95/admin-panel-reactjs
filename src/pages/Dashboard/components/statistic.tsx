import React from "react";
import { Grid, Icon, Paper, Typography } from "@material-ui/core";
import { useStatisticStyle } from "../dashboard";

export const StatisticCard: React.FC = () => {
  const classes = useStatisticStyle();
  const Mock = [1, 2, 3, 4];
  return (
    <>
      <Grid container spacing={3}>
        {Mock.map(e => (
          <Grid key={e} item xs={6} md={3}>
            <Paper className={classes.paper}>
              <Grid
                container
                justify={"center"}
                alignItems={"center"}
                direction={"row"}
              >
                <Icon fontSize={"large"}>spellcheck</Icon>
                <div>
                  <Typography variant={"body2"}>Words</Typography>
                  <Typography variant={"h5"}>1,356</Typography>
                </div>
              </Grid>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </>
  );
};
