import React from 'react';
import {StatisticCard} from './components/statistic';
import DataTable from '../../shared/dataTable';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {Grid} from '@material-ui/core';
import {SettingsCard} from '../settings/settingsCard';

export const useStatisticStyle = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  })
);

export const Dashboard: React.FC<any> = (props: any) => {
  const classes = useStatisticStyle();
  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <SettingsCard />
        </Grid>
        <Grid item xs={12}>
          <StatisticCard />
        </Grid>
        <Grid item xs={12} md={6}>
          <DataTable dataTableTitle={'Words'} />
        </Grid>
        <Grid item xs={12} md={6}>
          <DataTable dataTableTitle={'Status codes'} />
        </Grid>
        <Grid item xs={12} md={6}>
          <DataTable dataTableTitle={'Links'} />
        </Grid>
        <Grid item xs={12} md={6}>
          <DataTable dataTableTitle={'FAQ'} />
        </Grid>
      </Grid>
    </div>
  );
};
