import React from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import {red} from '@material-ui/core/colors';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {Grid} from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Drawer from '@material-ui/core/Drawer';
import {MyEditor} from '../../shared/editor';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  })
);

export const VacancyPage: React.FC = () => {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  type DrawerSide = 'top' | 'left' | 'bottom' | 'right';
  const classes = useStyles();
  const toggleDrawer = (side: DrawerSide, open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
    if (event.type === 'keydown' && ((event as React.KeyboardEvent).key === 'Tab' || (event as React.KeyboardEvent).key === 'Shift')) {
      return;
    }

    setState({...state, [side]: open});
  };

  return (
    <>
      <Drawer anchor="top" open={state.top} onClose={toggleDrawer('top', false)}>
        <MyEditor />
      </Drawer>
      <Grid container>
        <Grid item lg={6}>
          <Card>
            <CardHeader
              avatar={
                <Avatar aria-label="recipe" className={classes.avatar}>
                  FE
                </Avatar>
              }
              action={
                <IconButton aria-label="settings">
                  <MoreVertIcon />
                </IconButton>
              }
              title="Front-end Developer"
              subheader="March 14, 2020"
            />
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p">
                Angular 2+, React js, Node js, OOP, Agile, Scrum
              </Typography>
            </CardContent>
            <CardActions disableSpacing>
              <Box width={'100%'} display="flex" justifyContent="flex-end">
                <IconButton color={'secondary'}>
                  <DeleteIcon />
                </IconButton>
                <IconButton onClick={toggleDrawer('top', true)} color={'primary'}>
                  <EditIcon />
                </IconButton>
              </Box>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </>
  );
};
