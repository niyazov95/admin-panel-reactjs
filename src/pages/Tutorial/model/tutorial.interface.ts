export interface ITutorial {
  videoUrl: string;
  tutorialName: string;
  tutorialDescription: string;
  collapsed?: boolean;
}
