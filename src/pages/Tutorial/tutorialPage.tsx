import React from 'react';
import {createStyles, Grid, Theme} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import {VIDEO_MOCK} from './const/video-mock.const';
import {ITutorial} from './model/tutorial.interface';
import Collapse from '@material-ui/core/Collapse';
import TextField from '@material-ui/core/TextField';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    textField: {
      width: '100%',
      margin: '10px 0',
    },
  })
);

export const TutorialPage: React.FC = () => {
  const classes = useStyles();
  const tutorials: ITutorial[] = VIDEO_MOCK;
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = (tutorial: ITutorial) => () => {
    tutorial.collapsed = !tutorial.collapsed;
    setExpanded(!expanded);
  };
  return (
    <>
      <Grid container spacing={3}>
        {tutorials.map(tutorial => (
          <Grid item md={4} xs={6} key={tutorial.tutorialName}>
            <Card>
              <CardActionArea>
                <CardMedia src={tutorial.videoUrl} component="iframe" height="300" />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {tutorial.tutorialName}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {tutorial.tutorialDescription}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Box display="flex" width="100%" justifyContent="flex-end">
                  <Button size="small" color="secondary">
                    Delete
                  </Button>
                  <Button
                    size="small"
                    color="primary"
                    variant={tutorial.collapsed ? 'contained' : 'text'}
                    onClick={handleExpandClick(tutorial)}
                    aria-expanded={tutorial.collapsed}>
                    {tutorial.collapsed ? 'Save' : 'Edit'}
                  </Button>
                </Box>
              </CardActions>
              <Collapse in={tutorial.collapsed} timeout="auto" unmountOnExit>
                <CardContent>
                  <form noValidate autoComplete="off">
                    <TextField className={classes.textField} id="standard-basic" label="Tutorial Name" />
                    <TextField className={classes.textField} id="standard-basic" label="Tutorial Description" multiline rows={3} />
                    <TextField className={classes.textField} id="standard-basic" label="Video Url" />
                  </form>
                </CardContent>
              </Collapse>
            </Card>
          </Grid>
        ))}
      </Grid>
    </>
  );
};
