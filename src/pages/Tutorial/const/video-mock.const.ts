import {ITutorial} from '../model/tutorial.interface';

export const VIDEO_MOCK: ITutorial[] = [
  {
    videoUrl: 'https://www.youtube.com/embed/mUvGqo6Vvlw',
    tutorialName: 'TutorialName 1',
    tutorialDescription: 'Tutorial Description 1',
  },
  {
    videoUrl: 'https://www.youtube.com/embed/mUvGqo6Vvlw',
    tutorialName: 'TutorialName 2',
    tutorialDescription: 'Tutorial Description 2',
  },
  {
    videoUrl: 'https://www.youtube.com/embed/mUvGqo6Vvlw',
    tutorialName: 'TutorialName 3',
    tutorialDescription: 'Tutorial Description 3',
  },
  {
    videoUrl: 'https://www.youtube.com/embed/mUvGqo6Vvlw',
    tutorialName: 'TutorialName 4',
    tutorialDescription: 'Tutorial Description 4',
  },
];
