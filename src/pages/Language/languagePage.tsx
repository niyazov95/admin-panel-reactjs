import React from 'react';
import {Theme, createStyles, makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import {MOCK_LANG} from '../settings/mock/mock';
import {Switch} from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    icon: {
      verticalAlign: 'bottom',
      height: 20,
      width: 20,
    },
    details: {
      alignItems: 'center',
    },
    column: {
      flexBasis: '33.33%',
    },
    helper: {
      borderLeft: `2px solid ${theme.palette.divider}`,
      padding: theme.spacing(1, 2),
    },
    link: {
      color: theme.palette.primary.main,
      textDecoration: 'none',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
  })
);

export const LanguagePage: React.FC = () => {
  const classes = useStyles();
  const languages = MOCK_LANG;
  const [checked, setChecked] = React.useState([]);
  const changeLangActivity = (language: any) => () => {
    language.active = !language.active;
    const newChecked = [...checked];
    setChecked(newChecked);
  };
  const changeDefaultLang = (lang: any) => () => {
    languages.forEach(l => (l.default = false));
    lang.default = !lang.default;
    const newChecked = [...checked];
    setChecked(newChecked);
  };

  return (
    <div className={classes.root}>
      {languages.map(lang => (
        <ExpansionPanel defaultExpanded key={lang.languageName}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1c-content" id="panel1c-header">
            <div className={classes.column}>
              <Typography className={classes.heading}>Language: {lang.languageName}</Typography>
            </div>
            <div className={classes.column}>
              <Typography className={classes.secondaryHeading}>{lang.active ? 'Active' : 'Non Active'}</Typography>
            </div>
            <div className={classes.column}>
              <Typography className={classes.secondaryHeading}>{lang.default ? 'Default' : 'Non Default'}</Typography>
            </div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.details}>
            <div className={classes.column}>
              <FormControlLabel
                label="Language Activity"
                control={<Switch checked={lang.active} onChange={changeLangActivity(lang)} value="{lang.active}" color="primary" />}
              />
            </div>
            <div className={classes.column}>
              <FormControlLabel
                label="Default Language"
                control={<Switch checked={lang.default} onChange={changeDefaultLang(lang)} value="{lang.default}" color="primary" />}
              />
            </div>
            <div className={clsx(classes.column, classes.helper)}>
              <Typography variant="caption">
                Select your language settings
                <br />
                <a href="#secondary-heading-and-columns" className={classes.link}>
                  Learn more
                </a>
              </Typography>
            </div>
          </ExpansionPanelDetails>
          <Divider />
          <ExpansionPanelActions>
            <Button size="small">Cancel</Button>
            <Button size="small" color="primary">
              Save
            </Button>
          </ExpansionPanelActions>
        </ExpansionPanel>
      ))}
    </div>
  );
};
