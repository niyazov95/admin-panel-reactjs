import React, {useEffect} from 'react';
import MaterialTable, {Column} from 'material-table';
import {apiGet} from './services/apiService';

interface Row {
  title: string;
  AZ: string;
  EN: string;
  date: number;
}

interface TableState {
  columns: Array<Column<Row>>;
  data: Row[];
}

interface IDataTable {
  dataTableTitle: string;
}

export default function DataTable(props: IDataTable) {
  const cellStyle = {
    maxWidth: '200px',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  };
  const onRowAdd = (newData): Promise<any> => {
    console.log(newData);
    return new Promise(resolve => {
      setTimeout(() => {
        resolve();
        setState(prevState => {
          const data = [...prevState.data];
          data.push(newData);
          return {...prevState, data};
        });
      }, 600);
    });
  };
  const onRowUpdate = (newData, oldData): Promise<any> => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve();
        if (oldData) {
          setState(prevState => {
            const data = [...prevState.data];
            data[data.indexOf(oldData)] = newData;
            return {...prevState, data};
          });
        }
      }, 600);
    });
  };
  const onRowDelete = (oldData): Promise<any> => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve();
        setState(prevState => {
          const data = [...prevState.data];
          data.splice(data.indexOf(oldData), 1);
          return {...prevState, data};
        });
      }, 600);
    });
  };
  const [state, setState] = React.useState<TableState>({
    columns: [
      {title: 'Keyword', field: 'keyword', cellStyle: cellStyle},
      {title: 'Azerbaijan', field: 'AZ', cellStyle: cellStyle},
      {title: 'English', field: 'EN', cellStyle: cellStyle},
      {title: 'Date', field: 'date', type: 'numeric'},
    ],
    data: [
      {title: 'close', EN: 'Close', AZ: 'Bagla', date: 2020},
      {title: 'send', EN: 'Send', AZ: 'Gonder', date: 2019},
      {title: 'close', EN: 'Close', AZ: 'Bagla', date: 2020},
      {title: 'send', EN: 'Send', AZ: 'Gonder', date: 2019},
      {title: 'close', EN: 'Close', AZ: 'Bagla', date: 2020},
      {title: 'send', EN: 'Send', AZ: 'Gonder', date: 2019},
    ],
  });
  useEffect(() => {
    apiGet('http://92.204.4.81:3001/translations').then(r => (state.data = r));
  }, [state.data]);
  return (
    <MaterialTable
      title={props.dataTableTitle}
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData => onRowAdd(newData),
        onRowUpdate: (newData, oldData) => onRowUpdate(newData, oldData),
        onRowDelete: oldData => onRowDelete(oldData),
      }}
    />
  );
}
