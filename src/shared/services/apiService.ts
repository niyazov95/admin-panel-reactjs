import axios, {AxiosResponse} from 'axios';
const processRequest = <T>(request: Promise<AxiosResponse<T>>) => request.then(response => response.data);
export class ApiService {
  get(url) {
    return axios.get(url);
  }
}

export const apiGet = <T>(url): Promise<T> => processRequest<T>(axios.get<T>(url));

export const apiPost = <T, R>(url, requestBody: R): Promise<T> => processRequest<T>(axios.post<T>(url, requestBody));

