import React from 'react';
import {Editor} from '@tinymce/tinymce-react';

export const MyEditor: React.FC = () => {
  const handleEditorChange = (content, editor) => {
    console.log('Content was updated:', content);
  };

  return (
    <>
      <Editor
        apiKey="0ka01bzyxw01uqlpz1z081w3ofamu8wffb9v68jfg7cq4jxz"
        initialValue="<p>Start typing</p>"
        init={{
          height: 500,
          menubar: true,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount',
          ],
          toolbar:
            'undo redo | formatselect | bold italic backcolor | \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | removeformat | help',
        }}
        onEditorChange={handleEditorChange}
      />
    </>
  );
};
